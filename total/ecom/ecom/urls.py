"""ecom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from cart import views
from cart.views import ListUsers,  CustomAuthToken
from cart2.views import RegisterAPI
from knox import views as knox_views
from cart2.views import LoginAPI
from django.urls import path

urlpatterns = [
     path('api/users/', ListUsers.as_view()),
    path('admin/', admin.site.urls),
    path('api/token/auth/', CustomAuthToken.as_view()),

    path('api/register/', RegisterAPI.as_view(), name='register'),
    path('api/login/', LoginAPI.as_view(), name='login'),
    path('api/logout/', knox_views.LogoutView.as_view(), name='logout'),
    path('api/logoutall/', knox_views.LogoutAllView.as_view(), name='logoutall'),



    # products
    path("product_list/", views.productList.as_view()),
    path("product_details/<int:pk>/", views.productDetails.as_view()),
    # orders
    path("orders_list/", views.orderList.as_view()),
    path("orders_details/<int:pk>/", views.orderDetails.as_view()),

    # orders_items
    path("items_list/", views.orders_items_List.as_view()),
    path("items_details/<int:pk>/", views.orders_items_Details.as_view()),


]

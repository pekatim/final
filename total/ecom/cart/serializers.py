from rest_framework import serializers
from cart.models import products, ordersItems, orders
from django.contrib.auth.models import User


from django.contrib.auth.models import User
from rest_framework import serializers



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'last_name','first_name']






class product_Serializer(serializers.ModelSerializer):
    class Meta:
        model = products
        fields = ('id', 'Title', 'Description', 'Image', 'Price', 'Createdat', 'Updatedat')


class orders_Serializer(serializers.ModelSerializer):
    class Meta:
        model = orders
        fields = ('id', 'User_id', 'Total', 'Createdat', 'Updatedat', 'Status', 'mode_of_payment')


class orders_items_Serializer(serializers.ModelSerializer):
    class Meta:
        model = ordersItems
        fields = ('id', 'Order_id', 'Product_id', 'Quantity', 'Total_Price')

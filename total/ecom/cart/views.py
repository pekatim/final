from django.shortcuts import render
from django.contrib.auth.models import User
from knox.models import AuthToken
from rest_framework import generics
from rest_framework.authentication import TokenAuthentication

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from stripe import Order

from cart.models import products, ordersItems, ordersItems, orders
from cart.serializers import orders_items_Serializer, product_Serializer, orders_Serializer,UserSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from rest_framework.exceptions import PermissionDenied


# Create your views here.

class ListUsers(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes=[permissions.IsAuthenticated]
    def get(self,request,format=None):
        usernames=[user.username for  user in User.objects.all()]
        return Response(usernames)


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })









# products
class productList(generics.ListCreateAPIView):
    queryset = products.objects.all()
    serializer_class = product_Serializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]


class productDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = products.objects.all()
    serializer_class = product_Serializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticatedOrReadOnly]


# orders
class orderList(generics.ListCreateAPIView):
    queryset = orders.objects.all()
    serializer_class = orders_Serializer





class orderDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = orders.objects.all()
    serializer_class = orders_Serializer








# order items

class orders_items_List(generics.ListCreateAPIView):
    queryset = ordersItems.objects.all()
    serializer_class = orders_items_Serializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]


class orders_items_Details(generics.RetrieveUpdateDestroyAPIView):
    queryset = ordersItems.objects.all()
    serializer_class = orders_items_Serializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticatedOrReadOnly]

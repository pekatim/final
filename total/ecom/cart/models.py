from django.db import models


from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

class products(models.Model):
    Title = models.CharField(max_length=20)
    Description = models.CharField(max_length=30)
    Image = models.ImageField()
    Price = models.FloatField()
    Createdat = models.DateField(null=True)
    Updatedat = models.DateField(null=True)

    def _str_(self):
        return f"{self.id} - {self.Title} - {self.Description} - {self.Image} - {self.Price} - {self.Createdat}- {self.Updatedat} "


class orders(models.Model):
    pay_choices = (
        ('cash', 'C'),
        ('online payment', 'ON'),
    )

    status_choices = (
        ('notpaid', 'NP'),
        ('paid', 'P'),

    )
    User_id = models.ForeignKey(products, on_delete=models.CASCADE)
    Total = models.IntegerField()
    Createdat = models.DateField(null=True)
    Updatedat = models.DateField(null=True)
    Status = models.CharField(max_length=20, choices=status_choices)
    mode_of_payment = models.CharField(max_length=30, choices=pay_choices)

    def _str_(self):
        return f"{self.id}-{self.User_id} - {self.Total} - {self.Createdat} - {self.Updatedat} - {self.Status} - {self.mode_of_payment} "


class ordersItems(models.Model):
    Order_id = models.ForeignKey(orders, on_delete=models.CASCADE)
    Product_id = models.ForeignKey(products, on_delete=models.CASCADE)
    Quantity = models.IntegerField()
    Total_Price = models.FloatField()

    def _str_(self):
        return f"{self.Order_id} - {self.Product_id} - {self.Quantity} - {self.Total_Price} "

class UserProfile(models.Model):
    first_name = models.CharField(max_length=150)
    last_name =  models.CharField(max_length=150)
    email = models.EmailField(max_length=150)
    password = models.CharField(max_length=150)


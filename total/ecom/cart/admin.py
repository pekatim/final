from django.contrib import admin

from cart.models import products,orders,ordersItems


admin.site.register(products)
admin.site.register(orders)
admin.site.register(ordersItems)
# Register your models here.

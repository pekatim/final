import django.forms as forms
from .models import products, orders, ordersItems

from django.contrib.auth.models import User
from django.contrib.auth.models import UserCreationForm


class Products_Form(forms.ModelForm):
    class Meta:
        model = products
        fields = '_all_'


class orders_Form(forms.ModelForm):
    class Meta:
        model = orders
        fields = '_all_'


class orders_items_Form(forms.ModelForm):
    class Meta:
        model = ordersItems
        fields = '_all_'

class SignUpForm(UserCreationForm):

    class Meta:
        model = User

        fields = ('first_name',
                  'last_name',
                  'email',
                  'password1',
                  'password2', )
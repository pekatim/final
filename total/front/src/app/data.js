const BELTS = [
  {
    ref: "BELTS_1",
    category: 0,
    name: "FASTRACK BELT",
    price: 1999,
    unit: "1",
    image: "https://static.toiimg.com/photo/72975551.cms"
  },
  {
    ref: "BELTS_2",
    category: 0,
    name: "WRANGLER BELT",
    price: 1599,
    unit: "1",
    image: "wm.jpeg"
  },
  {
    ref: "BELTS_3",
    category: 0,
    name: "UCB BELTS",
    price: 2999,
    unit: "1",
    image: "ub.jpeg"
  },
  {
    ref: "BELTS_4",
    category: 0,
    name: "TOMMYHILFIGER",
    price: 3999,
    unit: "1",
    image: "tb.jpeg"
  },
  {
    ref: "BELTS_5",
    category: 0,
    name: "LP",
    price: 4999,
    unit: "1",
    image: "lb.jpeg"
  },
  {
    ref: "BELTS_6",
    category: 0,
    name: "BLACKBERRY",
    price: 5099,
    unit: "1",
    image: ""
  },
  ,
  {
    ref: "BELTS_7",
    category: 0,
    name: "ALLENSOLLY",
    price: 4659,
    unit: "1",
    image: "ab.jpeg"
  },
  {
    ref: "BELTS_8",
    category: 0,
    name: "LEVIS",
    price: 4509,
    unit: "1",
    image: "ab.jpeg"
  },
  {
    ref: "BELTS_9",
    category: 0,
    name: "US POLO",
    price: 3949,
    unit: "1",
    image: "ubb.jpeg"
  }
];

const SHOES = [
  {
    ref: "SHOES_1",
    category: 1,
    name: "LEE COPPER",
    price: 2999,
    unit: "1-PAIR",
    image: "ls"
  },
  {
    ref: "SHOES_2",
    category: 1,
    name: "WOODLAND",
    price: 3999,
    unit: "1-PAIR",
    image: "https://5.imimg.com/data5/YV/WJ/MY-4390697/sports-watch-500x500.jpg"
  },
  {
    ref: "SHOES_3",
    category: 1,
    name: "RED TAPE",
    price: 3999,
    unit: "1-PAIR",
    image: "r.jpeg"
  },
  {
    ref: "SHOES_4",
    name: "TOMMY ",
    category: 1,
    price: 5000,
    unit: "1-PAIR",
    image: "t.jpeg"
  },
  {
    ref: "SHOES_5",
    name: "BATA",
    category: 1,
    price: 3699,
    unit: "1",
    image: "b.jpeg"
  },
  {
    ref: "SHOES_6",
    name: "VANHUSEN",
    category: 1,
    price: 5999,
    unit: "1-PAIR",
    image: "v.jpeg"
  },
  {
    ref: "SHOES_7",
    name: "NIKE",
    category: 1,
    price: 20000,
    unit: "1-PAIR",
    image: "n.jpeg"
  },
  {
    ref: "SHOES_8",
    name: "ADIDAS",
    category: 1,
    price: 10999,
    unit: "1-PAIR",
    image: "add.jpeg"
  }
];

const WALLETS = [
  {
    ref: "WALLETS_1",
    category: 2,
    name: "FASTRACK",
    price: 2099,
    unit: "1",
    image: "fw.jpeg"
  },
  {
    ref: "WALLETS_2",
    category: 2,
    name: "LOUIS PHILPIE",
    price: 1999,
    unit: "1",
    image: "lw.jpeg"
  },
  {
    ref: "WALLETS_3",
    category: 2,
    name: "WOODLAND",
    price: 3599,
    unit: "1",
    image: "wb.jpeg"
  },
  {
    ref: "WALLETS_4",
    category: 2,
    name: "BATA",
    price: 13999,
    unit: "1",
    image: "bw.jpeg"
  },
  {
    ref: "WALLETS_5",
    category: 2,
    name: "URBAN",
    price: 10.19,
    unit: "pièce",
    image: "uw.jpeg"
  }
];

const SPRAYS = [
  {
    ref: "SPRAYS_1",
    category: 3,
    name: "ONE8",
    price: 299,
    unit: "1",
    image: "1.jpeg"
  },
  {
    ref: "SPRAYS_2",
    category: 3,
    name: "WILDSTONE",
    price: 199,
    unit: "1",
    image: "2.jpeg"
  },
  {
    ref: "SPRAYS_3",
    category: 3,
    name: "EVA",
    price: 399,
    unit: "1",
    image: "3.jpeg"
  },
  {
    ref: "SPRAYS_4",
    category: 3,
    name: "AXE",
    price: 599,
    unit: "1",
    image: "4.jpeg"
  },
  {
    ref: "SPRAYS_5",
    category: 3,
    name: "CINTHOL POCKET SPRAY",
    price: 359,
    unit: "1",
    image: "5.jpeg"
  },
  {
    ref: "SPRAYS_6",
    category: 3,
    name: "FOG",
    price: 369,
    unit: "1",
    image: "6.jpeg"
  },
  {
    ref: "SPRAYS_7",
    category: 3,
    name: "DENVER",
    price: 959,
    unit: "1",
    image: "7.jpeg"
  }
];

const WATCHES = [
  {
    ref: "WATCHES_1",
    category: 4,
    name: "FASTRACK",
    price: 1299,
    unit: "1",
    image: "1a.jpeg"
  },
  {
    ref: "WATCHES_2",
    category: 4,
    name: "CASIO",
    price: 11999,
    unit: "1",
    image: "2a.jpeg"
  },
  {
    ref: "WATCHES_3",
    category: 4,
    name: "ROLLEX",
    price: 2533399,
    unit: "1",
    image: "3a.jpeg"
  },
  {
    ref: "WATCHES_4",
    category: 4,
    name: "REALME ",
    price: 2549,
    unit: "1",
    image: "4a.jpeg"
  },
  {
    ref: "WATCHES_5",
    category: 4,
    name: "MI",
    price: 2999,
    unit: "1",
    image: "5a.jpeg"
  },
  {
    ref: "WATCHES_6",
    category: 4,
    name: "FASTRACK REFLEX",
    price: 3449,
    unit: "1",
    image: "6a.jpeg"
  },
  {
    ref: "WATCHESs_7",
    category: 4,
    name: "TITAN",
    price: 7819,
    unit: "1",
    image: "7a.jpeg"
  },
  {
    ref: "WATCHES_8",
    category: 4,
    name: "TIMEWELL",
    price: 5449,
    unit: "1",
    image: "8a.jpeg"
  },
  {
    ref: "WATCHES_9",
    category: 4,
    name: "SONATA",
    price: 3449,
    unit: "1",
    image: "9a.jpeg"
  }
];

export const list = [BELTS, SHOES, WALLETS, SPRAYS, WATCHES];
